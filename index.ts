import * as os from 'os';
import WebSocket from 'ws';
import { machineIdSync } from 'node-machine-id';

const url = 'wss://spring.raph.workers.dev/test/device';
const uniqueid = machineIdSync(false);
const secondsBetweenRefresh = 2;

const send = (ws: WebSocket, msg: any) => {
    console.log('Sending', msg);
    ws.send(JSON.stringify(msg));
}

const getCpu = () => os.cpus().reduce((prev, cpu) => prev + cpu.times.user, 0);

const main = async (ws: WebSocket) => {
    const totalmemory = os.totalmem();
    send(ws, {
        hostname: os.hostname(),
        arch: os.arch(),
        platform: os.platform(),
        type: os.type(),
        version: os.version(),
        cores: os.cpus().length,
        total_memory: totalmemory
    });

    let oldCpu = getCpu();
    while (true) {
        const cpu = getCpu();

        const device = {
            cpu: cpu - oldCpu,
            memory: totalmemory - os.freemem(),
        }
        oldCpu = cpu;

        send(ws, device);

        await new Promise(r => setTimeout(r, secondsBetweenRefresh * 1000));
    }
}

const websocket = new WebSocket(url + '/' + uniqueid);
websocket.onopen = () => main(websocket);
